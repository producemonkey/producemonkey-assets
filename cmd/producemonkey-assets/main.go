package main

import (
	"fmt"
	"log"

	"gitlab.com/producemonkey/producemonkey-assets/db"
)

func main() {
	if err := db.DLImages(); err != nil {
		log.Fatal(fmt.Errorf("error: %w", err))
	}
}
