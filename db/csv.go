//
//  PRODUCE MONKEY CONFIDENTIAL
//  _________________
//   2021- 2022 PRODUCE MONKEY
//   All Rights Reserved.
//
//   NOTICE:  All information contained herein is, and remains
//   the property of PRODUCE MONKEY and its suppliers,
//   if any.  The intellectual and technical concepts contained
//   herein are proprietary to PRODUCE MONKEY
//   and its suppliers and may be covered by U.S. and Foreign Patents,
//   patents in process, and are protected by trade secret or copyright law.
//   Dissemination of this information or reproduction of this material
//   is strictly forbidden unless prior written permission is obtained
//   from PRODUCE MONKEY.
//

package db

import (
	"embed"
	"fmt"
)

//go:embed csv/*.csv
var f embed.FS

// Read all CSV into array of []bytes
func CsvImport() ([][]byte, error) {

	dirArr, err := f.ReadDir("csv")
	if err != nil {
		return nil, fmt.Errorf("could not read directory: %w", err)
	}

	var csvList [][]byte

	for i := 0; i < len(dirArr); i++ {
		csv, err := f.ReadFile("csv/" + dirArr[i].Name())
		if err != nil {
			return nil, fmt.Errorf("could not read csv file: %w", err)
		}

		csvList = append(csvList, csv)
	}

	return csvList, nil
}
