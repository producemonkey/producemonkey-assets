//
//  PRODUCE MONKEY CONFIDENTIAL
//  _________________
//   2021- 2022 PRODUCE MONKEY
//   All Rights Reserved.
//
//   NOTICE:  All information contained herein is, and remains
//   the property of PRODUCE MONKEY and its suppliers,
//   if any.  The intellectual and technical concepts contained
//   herein are proprietary to PRODUCE MONKEY
//   and its suppliers and may be covered by U.S. and Foreign Patents,
//   patents in process, and are protected by trade secret or copyright law.
//   Dissemination of this information or reproduction of this material
//   is strictly forbidden unless prior written permission is obtained
//   from PRODUCE MONKEY.
//

package db

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/gocarina/gocsv"
)

type csv struct {
	ImageSrc  string `csv:"image-src"`
	Variety   string `csv:"variety"`
	Commodity string `csv:"commodity"`
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}

func writePNG(filePath string, imgData io.ReadCloser) error {

	defer imgData.Close()

	file, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("could not create file: %w", err)
	}

	defer file.Close()

	_, erro := io.Copy(file, imgData)
	if erro != nil {
		return fmt.Errorf("could not copy data into file: %w", err)
	}

	return nil
}

// shows list of image-src in csv files
func DLImages() error {
	csvFiles, err := CsvImport()
	if err != nil {
		return fmt.Errorf("could not import CSV files: %w", err)
	}

	for _, file := range csvFiles {
		var c []csv
		if err := gocsv.UnmarshalBytes(file, &c); err != nil {
			return fmt.Errorf("could not unmarshal csv: %w", err)
		}
		for _, uri := range c {
			// download commodity image
			CommodityName := strings.TrimSpace(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(strings.ToLower(uri.Commodity), " / ", "-"), ", ", "-"), " ", "-"))
			if !fileExists("images/commodities/" + CommodityName + ".png") {
				linkCommodities := "https://www.producemarketguide.com/sites/default/files/Commodities.tar/Commodities/" + CommodityName + "_commodity-page.png"

				respCommodities, err := http.Get(linkCommodities)
				if err != nil {
					return fmt.Errorf("could not get commodity image from provided url: %w", err)
				}

				if err := writePNG("images/commodities/"+CommodityName+".png", respCommodities.Body); err != nil {
					return fmt.Errorf("could not write to commodity file: %w", err)
				}
			}
			if uri.Variety != "" {

				// download variety image
				linkVarieties := "https://www.producemarketguide.com" + uri.ImageSrc
				fileNameVarieties := "images/" + strings.TrimSpace(strings.ReplaceAll(strings.ReplaceAll(strings.ToLower(uri.Variety), "/", "-"), " ", "-")) + ".png"

				respVarieties, err := http.Get(linkVarieties)
				if err != nil {
					return fmt.Errorf("could not get image from provided url: %w", err)
				}

				if err := writePNG(fileNameVarieties, respVarieties.Body); err != nil {
					return fmt.Errorf("could not write to file: %w", err)
				}
			}
		}
	}

	return nil
}
